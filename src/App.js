import React from 'react';
import {
  Row,
  Col
}from'react-bootstrap';
import './App.css';

function Header(){
  return (
    <div>
      <ul className="nav justify-content-end">
        <li className="nav-item">
          <a className="nav-link active" aria-current="page" href="#">My h&m</a>
        </li>
        <li className="nav-item">
          <a className="nav-link link" href="#">Sign In/Join</a>
        </li>
        <li className="nav-item">
          <a className="nav-link link" href="#"  style={{fontWeight:"bold"}}><img src="https://icon-library.com/images/black-heart-icon-png/black-heart-icon-png-28.jpg" width="20px" />Favorites</a>
        </li>
        <li className="nav-item">
          <a className="nav-link link" href="#"  style={{fontWeight:"bold"}}><img src="https://image.flaticon.com/icons/png/512/44/44338.png" width="16px" />Shopping bag</a>
        </li>
      </ul>
      <div className="text-center">
        <a className="navbar-brand " href="#">
          <img src="data:image/svg+xml;charset=US-ASCII,%3Csvg%20width%3D%2232%22%20height%3D%2221%22%20viewBox%3D%220%200%2032%2021%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%3E%3Cg%20fill%3D%22%23CD1F26%22%20fill-rule%3D%22evenodd%22%3E%3Cpath%20d%3D%22M29.272.953c-.506.225-.9-.01-1.517%201.257-1.94%203.987-3.45%207.223-5.25%2011.88.053-3.824.737-6.868%201.145-10.59.075-.688.18-1.633.165-2.346-.012-.548-.558-.62-1.658-.078-.612.302-1.348.277-1.63%201.004-1.195%203.097-3.956%2011.707-5.238%2016.576-.31%201.17.85%201.366%201.197.166.843-2.9%202.77-9.323%203.917-12.63-.115%203.028-.94%207.813-.555%2010.324.12.79.857%201.13%201.236%201.8.27.475.703.523.94-.127%201.123-3.064%203.308-8.464%205.416-12.784-.413%201.9-1.998%209.97-2.44%2013.187-.088.646.704.662%201.406%201.633.157.218.763.455.872-.103.375-1.93%202.822-14.458%203.78-18.438.258-1.072.184-1.612-1.79-.733m-16.005%209.104c.843-1.257%201.086-1.002%201.29-1.385.252-.477.085-.93-.817-.817%200%200-.34.025-.968.096%201-2.57%201.872-4.753%202.502-6.41.213-.565.24-.977-.108-1.06-.373-.09-1.45.492-2.115.606-.21.036-.414.26-.484.42A190.712%20190.712%200%200%200%209.728%208.39c-1.055.18-2.29.416-3.67.726.99-2.536%201.99-5.036%202.962-7.446C9.5.488%208.24.376%207.75%201.577a441.612%20441.612%200%200%200-3.148%207.88c-.97.24-1.996.51-3.07.82-.782.226-.816.434-.478.9.188.263.572.237.748.423.456.48.732%201.057%201.547%201.144a206.53%20206.53%200%200%200-2.115%205.88c-.413%201.21.764%201.46%201.217.177.73-2.065%201.498-4.157%202.288-6.243.658-.153%202.25-.496%203.723-.815-1.168%203.2-1.937%205.63-2.226%206.813-.056.227.037.353.084.446.396.574.77.597%201.278%201.306a.428.428%200%200%200%20.755-.178%20231.115%20231.115%200%200%201%203.23-9.08c.432-.095%201.207-.283%201.683-.99%22/%3E%3Cpath%20d%3D%22M14.062%2015.046a6.46%206.46%200%200%200-.236.22%2085.602%2085.602%200%200%201-.268-.837c.255-.314.5-.62.71-.946.868-1.352-.85-2.13-1.59-1.253-.394.47-.296.93-.187%201.323l.15.506c-.21.215-.463.486-.8.877-.84.974-.61%202.24.288%202.518.547.17%201.056-.082%201.498-.474.02.057.044.12.064.17.234.588.96.41.683-.3-.047-.12-.11-.294-.176-.484a9.27%209.27%200%200%200%20.388-.53c.535-.795.018-1.26-.523-.79zm-.57-1.86c-.08.092-.157.173-.233.252-.02-.063-.04-.134-.053-.184-.25-.944.832-.686.285-.068zm-.586%202.874c-.328.2-.59-.015-.148-.62.074-.103.152-.192.23-.29.073.227.15.457.227.685a2.694%202.694%200%200%201-.31.225z%22/%3E%3C/g%3E%3C/svg%3E" alt="" width="75" className="center-block"/>
        </a>
      </div>
    </div>
  );
}

  function Menu(){
    return (
      <div className="menu">
        <ul className="nav" style={{marginLeft:"400px"}}>
          <li className="nav-menu">
            <a className="nav-link menu-1" aria-current="page" href="#">LADIES</a>
          </li>
          <li className="nav-menu">
            <a className="nav-link menu-1" href="#">MEN</a>
          </li>
          <li className="nav-menu">
            <a className="nav-link menu-1" href="#">DIVIDED</a>
          </li>
          <li className="nav-menu">
            <a className="nav-link menu-1" href="#">KIDS</a>
          </li>
          <li className="nav-menu">
            <a className="nav-link menu-1" href="#">SALE</a>
          </li>
          <li className="nav-menu">
            <a className="nav-link menu-1" href="#">#HMxME</a>
          </li>
            <form className="d-flex ml-auto">
                <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
                <button className="btn btn-outline-light" type="submit" ><img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-search-strong-512.png"width="25px"/></button>
            </form>
        </ul>
      </div>
    );
  }

function Card(){
  return (
    <div className="text-center col-sm-9" style={{marginLeft:"150px"}}>
      <div className="card text-white mr-11 card-2 " style={{marginBottom:"30px"}}>
        <img src="https://d29c1z66frfv6c.cloudfront.net/pub/media/banner/W01_HOMEPAGE_LADIES-MEN-DIVIDED-KIDS_NOW_UP_TO_70_OFF_3x2.jpg" width="1000" />
        <div className="card-img-overlay" >
          <button type="button" className="btn btn-outline-light card-1">LADIES</button>
          <button type="button" className="btn btn-outline-light card-1">MEN</button>
          <button type="button" className="btn btn-outline-light card-1">DIVIDED</button>
          <button type="button" className="btn btn-outline-light card-1">KIDS</button>
        </div>              
      </div>
    </div>
  );
}

function Card2(){
  const Ulang = (props) =>{
    return(
         <div className="text-center col-sm-9" style={{marginLeft:"150px"}}>
            <div className="card text-white card-2">
              <img src={props.Image}  width="1000"/>
               <div className="card-img-overlay">
                <button className="btn btn-light btn-1" type="submit">Shop Now</button>
              </div>
          </div>
         </div>
    );
  }
  return(
      <div>
        <Ulang
          Image="https://d29c1z66frfv6c.cloudfront.net/pub/media/banner/W01_KIDS_MEET_Bottle2fashion_TITLE_CAMPAIGN.jpg"
        />
        <Ulang
          Image="https://d29c1z66frfv6c.cloudfront.net/pub/media/banner/W01_LADIES_TOTALLY_TAILORED.jpg"
        />
        <Ulang
          Image="https://d29c1z66frfv6c.cloudfront.net/pub/media/banner/W01_MEN_MULTIPACKS_FROM_IDR179900.jpg"
        />
        <Ulang
          Image="https://d29c1z66frfv6c.cloudfront.net/pub/media/banner/W01_DIVIDED_YOUR_NEW_GO_TO.jpg"
        />
      </div>
  );
}

function Footer(){
  return(
    <div style={{marginTop:"50px", backgroundColor:"black",height:"550px", paddingTop:"50px"}}>
      <Row>
        <Col>
          <ul style={{marginLeft:"200px"}}>
            <li style={{listStyleType:"none",marginBottom:"15px", fontSize:"20px"}}><a href="#" className="footer">SHOP</a></li> 
            <li style={{listStyleType:"none",marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Ladies</a></li> 
            <li style={{listStyleType:"none",marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Men</a></li>
            <li style={{listStyleType:"none",marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Divided</a></li>
            <li style={{listStyleType:"none",marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Kids</a></li>
          </ul>
        </Col>    
        <Col>
          <ul style={{marginLeft:"50px"}}>
            <li style={{listStyleType:"none",marginBottom:"15px", fontSize:"20px"}}><a href="#" className="footer">CORPORATE INFO</a></li> 
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Career at H&M</a></li> 
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">About H&M group</a></li>
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Sustainability</a></li>
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Investor Relations</a></li>
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Corporate Governance</a></li>
          </ul>
        </Col>
        <Col>
          <ul>
            <li style={{listStyleType:"none",marginBottom:"15px", fontSize:"20px"}}><a href="#" className="footer">HELP</a></li> 
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Customer Service</a></li> 
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">My H&M</a></li>
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Store Locator</a></li>
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Legal & Privacy</a></li>
            <li style={{listStyleType:"none", marginBottom:"5px", fontSize:"10px"}}><a href="#" className="footer">Contact</a></li>
          </ul>
        </Col> 
      </Row>
        <div className="text-center" style={{marginTop:"50px"}}>
          <img src="https://cdn.iconscout.com/icon/free/png-512/facebook-logo-2019-1597680-1350125.png" width="25px"  className="ikon" />
          <img src="https://pngimage.net/wp-content/uploads/2018/06/logo-line-hitam-putih-png-4.png" width="25px"  className="ikon" />
          <img src="https://www.freepnglogos.com/uploads/twitter-logo-png/twitter-logo-vector-png-clipart-1.png" width="25px"  className="ikon" />
          <img src="https://www.freepnglogos.com/uploads/logo-ig-png/logo-ig-lighting-and-furniture-design-studio-aqua-creations-32.png" width="25px"  className="ikon" />
          <img src="https://lh3.googleusercontent.com/proxy/4SHR_W2CxFzsMoVMuV05BgSnRAsufpQ57QtXuT9qoiEmFyXZDx5TEsjU3s2_bRIlkHb9gueqT4_3UVZeeyfcg0cSGzGBS5k" width="25px"  className="ikon" />
        </div>
          <p className="text">H&amp;M's business concept is to offer fashion and quality at the best price in a sustainable way. H&amp;M has since it was founded in 1947 grown into one of the world's leading fashion companies. The content of this site is copyright-protected and is the property of H&amp;M Hennes &amp; Mauritz AB.</p>
          <div className="text-center">
            <img src="https://1000logos.net/wp-content/uploads/2017/02/HM-symbol.jpg" width="80"/>
          </div>
        <p className="text-center">Indonesia | IDR</p>
    </div>
  
  );
}

function App(){
  return(
      <div>
        <Header/>
        <Menu/>
        <Card/>
        <Card2/>
        <Footer/>
      </div>
    );
}

export default App;